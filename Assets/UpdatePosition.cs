﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatePosition : MonoBehaviour {

    public Transform Transform;

	public void Update () {
        Transform.SetPositionAndRotation(Transform.localPosition, Transform.rotation);
	}
}
