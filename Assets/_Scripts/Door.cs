﻿using UnityEngine;

namespace Assets._Scripts
{
    public class Door : MonoBehaviour
    {
        public GameObject DoorObject;
        public GameObject[] BrokenDoorObjects;

        public void Start()
        {
            DoorObject.SetActive(true);
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != 9 || other.gameObject.GetComponentInParent<Rigidbody>().velocity.magnitude <= 10)
                return;

            DoorObject.gameObject.GetComponent<BoxCollider>().enabled = false;
            foreach(GameObject DoorPiece in BrokenDoorObjects)
            {
                DoorPiece.GetComponent<Rigidbody>().useGravity = true;
                DoorPiece.GetComponent<Rigidbody>().isKinematic = false;
            }
            AkSoundEngine.PostEvent("DoorBreak", gameObject);
        }

        public void Repair()
        {
            foreach(var go in BrokenDoorObjects)
            {
                var startingPosition = go.GetComponent<StartPosition>();
                var rb = go.GetComponent<Rigidbody>();
                rb.isKinematic = true;
                startingPosition.SetInitialPosition();
                DoorObject.gameObject.GetComponent<BoxCollider>().enabled = true;
            }


        }
    }
}