﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeStation : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)//Activate respective "HIT A" prompt
    {
        if(other.gameObject.tag == "Player")
        {
            other.GetComponentInChildren<LeafBlower>().CanRecharge();
            MainMenuManager.Instance.ToggleGaugePrompt(true);
        }
    }
    private void OnTriggerExit(Collider other)//Deactivate respective promt
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponentInChildren<LeafBlower>().CanRecharge();
            MainMenuManager.Instance.ToggleGaugePrompt(false);
        }
    }

}
