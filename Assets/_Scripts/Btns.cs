﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Btns : MonoBehaviour
{
    [SerializeField]
    private GameObject m_btn1;
    [SerializeField]
    private GameObject m_btn2;
    
    public void Start()
    {
        AkSoundEngine.StopAll();
        AkSoundEngine.PostEvent("StartMusicMenu", gameObject);
    }

    public void OnClickStart()
    {
        AkSoundEngine.PostEvent("GameStartSelected", gameObject);
        m_btn1.SetActive(false);
        m_btn2.SetActive(false);
        StartCoroutine(GameManager.Instance.CountDownBeforeGame("StartMusic1stRound"));
    }

    public void OnClickExit()
    {
        Application.Quit();
    }
}
