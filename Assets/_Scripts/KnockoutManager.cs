﻿using UnityEngine;

namespace Assets._Scripts
{
    public class KnockoutManager : MonoBehaviour
    {
        public RagdollManager RagdollManager;
        public AttackController AttackController;

        public bool IsKnockout;
        public float KnockoutCooldown = 5f;
        public float TimeStamp;

        public void Update()
        {
            if (IsKnockout && Time.time > TimeStamp)
            {
                IsKnockout = false;
                AttackController.enabled = true;
                RagdollManager.Desactivate();
            }
        }

        public void Knockout()
        {
            RagdollManager.Activate();
            AttackController.enabled = false;
            IsKnockout = true;
            TimeStamp = Time.time + KnockoutCooldown;
        }

        public void OnTriggerEnter(Collider other)
        {
            if ((other.gameObject.layer != 8 && other.gameObject.layer != 9) || other.gameObject.GetComponentInParent<Rigidbody>().velocity.magnitude <= 12)
                return;

            Knockout();
        }
    }
}
