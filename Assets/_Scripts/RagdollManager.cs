﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class RagdollManager : MonoBehaviour {

    public GameObject EntityWithRagdoll;
    public GameObject MainEntity;
    public Collider[] RagdollColliders;
    public Rigidbody[] RagdollRigidbodies;
    public Transform[] RagdollTransforms;
    public Dictionary<GameObject, Vector3> Positions = new Dictionary<GameObject, Vector3>();
    public Dictionary<GameObject, Quaternion> Rotations = new Dictionary<GameObject, Quaternion>();
    public UpdatePosition[] UpdatePosition;

	void Start () {
        RagdollColliders = EntityWithRagdoll.GetComponentsInChildren<Collider>();
        RagdollRigidbodies = EntityWithRagdoll.GetComponentsInChildren<Rigidbody>();
        RagdollTransforms = EntityWithRagdoll.GetComponentsInChildren<Transform>();
        foreach(var transform in RagdollTransforms)
        {
            Positions.Add(transform.gameObject, transform.localPosition);
            Rotations.Add(transform.gameObject, transform.localRotation);
        }

        Desactivate();
	}

	public virtual void Activate()
    {
        foreach(var c in RagdollColliders)
        {
            c.enabled = true;
        }

        foreach(var rb in RagdollRigidbodies)
        {
            rb.isKinematic = false;
            rb.useGravity = true;
        }

        foreach(var up in UpdatePosition)
        {
            up.enabled = true;
        }


        var animator = MainEntity.GetComponent<Animator>();
        var collider = MainEntity.GetComponent<Collider>();
        var rigidbody = MainEntity.GetComponent<Rigidbody>();
        var character = MainEntity.GetComponent<PlayerCharacter>();
        var controller = MainEntity.GetComponent<ThirdPersonUserControl>();
        if (animator != null)
            animator.enabled = false;
        if(collider != null)
            collider.enabled = false;
        if (rigidbody != null)
        {
            rigidbody.isKinematic = true;
            rigidbody.useGravity = false;
        }
        if (character != null)
            character.enabled = false;
        if (controller != null)
            controller.enabled = false;
    }

    public virtual void Desactivate()
    {
        foreach (var c in RagdollColliders)
        {
            c.enabled = false;
        }

        foreach (var rb in RagdollRigidbodies)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        foreach (var up in UpdatePosition)
        {
            up.enabled = false;
        }


        var animator = MainEntity.GetComponent<Animator>();
        var collider = MainEntity.GetComponent<Collider>();
        var rigidbody = MainEntity.GetComponent<Rigidbody>();
        var character = MainEntity.GetComponent<PlayerCharacter>();
        var controller = MainEntity.GetComponent<ThirdPersonUserControl>();
        if (animator != null)
            animator.enabled = true;
        if (collider != null)
            collider.enabled = true;
        if(rigidbody != null)
        {
            rigidbody.isKinematic = false;
            rigidbody.useGravity = true;
        }
        if (character != null)            
            character.enabled = true;
        if(controller != null)
            controller.enabled = true;
    }
}
