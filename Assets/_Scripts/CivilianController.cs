﻿using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

namespace Assets._Scripts
{
    public class CivilianController : MonoBehaviour
    {
        public RagdollManager RagdollManager;
        public ThirdPersonCharacter Controller;
        public Rigidbody Rigidbody;

        public void Start()
        {
            Rigidbody = GetComponent<Rigidbody>();
        }

        public void Update()
        {
            Controller.Move(transform.forward * 0.4f, false, false);

            if (Rigidbody.velocity.magnitude >= 5)
            {
                RagdollManager.Activate();
                AkSoundEngine.PostEvent("PasserbyFlyingWallah", gameObject);
                AkSoundEngine.PostEvent("Whoosh_ManThrown", gameObject);
            }
                
        }

        public void OnTriggerEnter(Collider other)
        {
            if ((other.gameObject.layer != 8 && other.gameObject.layer != 9) || other.gameObject.GetComponentInParent<Rigidbody>().velocity.magnitude <= 15)
                return;

            RagdollManager.Activate();
        }
    }
}
