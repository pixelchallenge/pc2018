﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderProperties : MonoBehaviour {

    public bool IsBigObject = false;
    public bool IsSmallObject = false;
    public bool IsSoftObject = false;
    public bool IsSteelTrashObject = false;
    public bool IsTransfo = false;

    private bool IsTrashCanMakingSound = false;
    public void MakeSounds ()
    {
		if(IsBigObject)
        {
            AkSoundEngine.PostEvent("HitBody_BigObject", gameObject);
        }
        else if(IsSmallObject)
        {
            AkSoundEngine.PostEvent("HitBody_SmallObject", gameObject);
        }
        else if(IsSoftObject)
        {
            AkSoundEngine.PostEvent("HitBody_SoftObject", gameObject);

        }
        else if(IsSteelTrashObject && !IsTrashCanMakingSound)
        {
            IsTrashCanMakingSound = true;
            AkSoundEngine.PostEvent("SteelTrash_CanFalls", gameObject);
            StartCoroutine(Waiting());
        }
        else if(IsTransfo)
        {
            AkSoundEngine.PostEvent("Transfo_Dropped", gameObject);
        }
    }
	
    private IEnumerator Waiting()
    {
        yield return new WaitForSeconds(0.75f);
        IsTrashCanMakingSound = false;
    }

}
