﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeafBlower : MonoBehaviour
{
    public Animator Animator;
    public Transform Player;
    public ParticleSystem ParticleSystem;
    public float m_BatteryCharge = 500f;
    public float m_Recharge = 25f;
    public float LeafBlowerAmplitude;
    public float LeafBlowerPower = 100f;
    public float UltimateAmount = 0;
    public float UltimateRequirement = 1;
    public float UltimateCooldown = 5f;
    public float UltimateTimestamp;
    public List<GameObject> ObjectsToPush;
    public string TriggerInput = "Player1 Right Trigger";
    public string AButtonInput = "Player1 Button A";
    public string XButtonInput = "Player1 Button X";
    public string TriggerPlayerRTPC = "TriggerPlayer1";
    public string TriggerPushed = "Blower1CharTriggerPushed";
    public string TriggerReleased = "Blower1CharTriggerReleased";
    public string EngineStarted = "Blower1EngineStarts";
    public string EngineStops = "Blower1EngineStops";

    public Slider m_SuperCharge;
    public Image m_SuperChargeFill;

    private bool m_CanRecharge = false;
    private bool _superChargeUsed = false;

    private bool EngineStart;


    public void Start()
    {
        ObjectsToPush = new List<GameObject>();
        Animator = GetComponentInParent<Animator>();
        
    }

    public void Update()
    {
        Debug.Log("UltimateAmmount: " + UltimateAmount);
        if (UltimateAmount >= UltimateRequirement && Input.GetButtonDown(XButtonInput))
        {
            Debug.Log("Supercharge used");
            _superChargeUsed = true;

            UltimateAmount = 0;
            m_SuperCharge.value = 0;

            //CoRoutine
            //m_SuperChargeFill.color = Color.red;



            UltimateTimestamp = Time.time + UltimateCooldown;
        }
        
        if (_superChargeUsed && Time.time >= UltimateTimestamp)
        {
            _superChargeUsed = false;
        }
        else
        {
            LeafBlowerPower = 250f;
        }

        LeafBlowerAmplitude = Math.Abs(Input.GetAxis(TriggerInput));
        Animator.SetFloat("Trigger", LeafBlowerAmplitude);
        
        AkSoundEngine.SetRTPCValue(TriggerPlayerRTPC, LeafBlowerAmplitude);

        //Should these be in the fixedupdate?
        if (LeafBlowerAmplitude >= 0.1f)
        {
            if(!EngineStart)
                AkSoundEngine.PostEvent(EngineStarted, gameObject);

            if (!ParticleSystem.isPlaying)
                ParticleSystem.Play();

            AkSoundEngine.PostEvent(TriggerPushed, gameObject);
            DepleteBattery(LeafBlowerAmplitude, TriggerInput == "Player1 Right Trigger" ? 1 : 2);
        }
        else
        {
            ParticleSystem.Stop();
        }

        if (m_CanRecharge && Input.GetButtonDown(AButtonInput))
        {
            RechargeBattery(AButtonInput == "Player1 Button A" ? 1 : 2);
        }
    }

    public void FixedUpdate()
    {
        //if (LeafBlowerPower <= 0.1 || m_BatteryCharge <= 0.1f)
        if (LeafBlowerAmplitude <= 0.1)
            return;

        ObjectsToPush.ForEach((t) =>
        {
            if (_superChargeUsed)
            {
                LeafBlowerPower = 1000f;
                //MainMenuManager.Instance.ConsumeCharge(m_SuperCharges);
            }
            var distance = Vector3.Distance(t.transform.position, transform.position);
            var rb = t.GetComponentInParent<Rigidbody>();
            if (rb != null && rb.velocity.magnitude > 15)
                return;
            if (rb != null && rb.isKinematic == true && !t.gameObject.name.Contains("Door"))
                rb.isKinematic = false;

            var force = (((Player.forward + new Vector3(0, 0.7f, 0)) * LeafBlowerPower * LeafBlowerAmplitude) / distance);
            rb.AddForceAtPosition(force, transform.position, ForceMode.Acceleration);

        });
    }

    public void DepleteBattery(float aAmplitude, int aPlayerNumber)
    {
        m_BatteryCharge -= aAmplitude;
        if (m_BatteryCharge <= 0)
            AkSoundEngine.PostEvent(EngineStops, gameObject);

        if (aPlayerNumber == 1)
        {
            //MainMenuManager.Instance.UpdateBatteryGauge1(m_BatteryCharge);
        }
        else if (aPlayerNumber == 2)
        {
            //MainMenuManager.Instance.UpdateBatteryGauge2(m_BatteryCharge);
        }
    }

    public void RechargeBattery(int aPlayerNumber)
    {
        if (m_BatteryCharge <= 0)
            AkSoundEngine.PostEvent(EngineStarted, gameObject);

        m_BatteryCharge += m_Recharge;

        if (aPlayerNumber == 1)
        {
            //MainMenuManager.Instance.UpdateBatteryGauge1(m_BatteryCharge);
        }
        else if (aPlayerNumber == 2)
        {
            //MainMenuManager.Instance.UpdateBatteryGauge2(m_BatteryCharge);
        }

    }

    public void CanRecharge()
    {
        m_CanRecharge = !m_CanRecharge;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (!ObjectsToPush.Exists(x => x == other.gameObject) && other.gameObject.layer == 9)
        {
            if (other.gameObject.name.Contains("Civil"))
                AkSoundEngine.PostEvent("PasserbyWallah", gameObject);

            ObjectsToPush.Add(other.gameObject);
        }
            
    }

    public void OnTriggerExit(Collider other)
    {
        if (ObjectsToPush.Contains(other.gameObject))
            ObjectsToPush.Remove(other.gameObject);
    }
}
