﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSoundDetection : MonoBehaviour {


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<ColliderProperties>() != null)
        {
            if (collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude >= 1f)
                collision.gameObject.GetComponent<ColliderProperties>().MakeSounds();
        }
    }
}
