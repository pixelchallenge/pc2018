﻿using System;
using TMPro;
using UnityEngine;

public class UIAnimationScript : MonoBehaviour {

    public TextMeshProUGUI TextMesh;
    public bool IsShowing = false;

    public float ShowSpeed = 10f;

    public void Start()
    {
        Hide();
    }

    public void Show()
    {
        gameObject.SetActive(true);
        IsShowing = true;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        IsShowing = false;
        TextMesh.color = new Color(TextMesh.color.r, TextMesh.color.g, TextMesh.color.b, 0);
    }

    public void Update()
    {
        if (!IsShowing)
            return;

        if (TextMesh.color.a < 2f)
        {
            TextMesh.color = new Color(TextMesh.color.r, TextMesh.color.g, TextMesh.color.b, TextMesh.color.a + (ShowSpeed * Time.deltaTime));
        }
        else
        {
            IsShowing = false;
            var gm = GameManager.Instance;
            TextMesh.text = String.Format("{0} : {1}", gm.PlayerOneRoundVictories.ToString(), gm.PlayerTwoRoundVictories.ToString());
        }
    }
}
