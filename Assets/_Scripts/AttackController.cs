﻿using System.Linq;
using UnityEngine;

namespace Assets._Scripts
{
    public class AttackController : MonoBehaviour
    {
        public Transform AttackPosition;
        public string AttackInput = "Player1 Attack";
        public float AttackCooldown = 8f;
        public float Timestamp;
        public bool IsOnCooldown;

        public void Update()
        {
            if (!IsOnCooldown && Input.GetButtonDown(AttackInput))
            {
                var colliders = Physics.OverlapSphere(AttackPosition.position, 1f);
                var player = colliders.FirstOrDefault(c => c.gameObject.layer == 8 && c.gameObject != gameObject);
                if (player != null)
                {
                    var ragdollManager = player.gameObject.GetComponentInChildren<KnockoutManager>();
                    ragdollManager.Knockout();
                    Timestamp = Time.time + AttackCooldown;
                    IsOnCooldown = true;
                }
            }

            if (IsOnCooldown && Time.time > Timestamp)
            {
                IsOnCooldown = false;
            }
        }
    }
}
