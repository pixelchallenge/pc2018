﻿using UnityEngine;

namespace Assets._Scripts
{
    public class Window : MonoBehaviour
    {
        public GameObject WindowObject;
        public GameObject BrokenWindowObject;

        public void Start()
        {
            WindowObject.SetActive(true);
            BrokenWindowObject.SetActive(false);
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != 9 || other.gameObject.GetComponentInParent<Rigidbody>().velocity.magnitude <= 10)
                return;

            WindowObject.SetActive(false);
            BrokenWindowObject.SetActive(true);
            AkSoundEngine.PostEvent("WindowBreak", gameObject);
        }

        public void Repair()
        {
            var startPositions = BrokenWindowObject.GetComponentsInChildren<StartPosition>();
            foreach (var sp in startPositions)
            {
                sp.SetInitialPosition();
            }
            BrokenWindowObject.SetActive(false);
            WindowObject.SetActive(true);
        }
    }
}
