﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{

    //do I need singleton?
    private static MainMenuManager instance;
    public static MainMenuManager Instance
    {
        get { return instance; }
    }


    [SerializeField]
    private Text m_Title;
    [SerializeField]
    private Text m_GaugePrompt1;
    [SerializeField]
    private Text m_GaugePrompt2;
    [SerializeField]
    private Button m_StartButton;
    [SerializeField]
    private Button m_OptionsButton;
    [SerializeField]
    private Text m_MusicText;
    [SerializeField]
    private Text m_SfxText;
    [SerializeField]
    private Slider m_SfxBar;
    [SerializeField]
    private Slider m_MusicBar;
    [SerializeField]
    private Slider m_BatteryGauge1;
    [SerializeField]
    private Slider m_BatteryGauge2;
    [SerializeField]
    private Image[] m_Charges1;
    [SerializeField]
    private Image[] m_Charges2;

    private bool m_InOptions = false;
    private string[] m_Gamepads;

    private void Awake ()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        Time.timeScale = 0;
	}

    public void UpdateBatteryGauge1(float a_BatteryLevel)//seperate to players later
    {
        m_BatteryGauge1.value = a_BatteryLevel;
    }

    public void UpdateBatteryGauge2(float a_BatteryLevel)//seperate to players later
    {
        
        m_BatteryGauge2.value = a_BatteryLevel;
    }

    public void StartGame()
    {
        foreach (Image charge in m_Charges1)
        {
            charge.gameObject.SetActive(true);
        }
        foreach (Image charge in m_Charges2)
        {
            charge.gameObject.SetActive(true);
        }
        m_BatteryGauge1.gameObject.SetActive(true);
        m_BatteryGauge2.gameObject.SetActive(true);
        m_OptionsButton.gameObject.SetActive(false);
        m_StartButton.gameObject.SetActive(false);
        m_SfxBar.gameObject.SetActive(false);
        m_MusicBar.gameObject.SetActive(false);
        m_MusicText.gameObject.SetActive(false);
        m_SfxText.gameObject.SetActive(false);

        Time.timeScale = 1;
    }

    public void ToggleOptionsMenu()
    {
        m_OptionsButton.gameObject.GetComponentInChildren<Text>().text = (m_InOptions ? "Options" : "Return");
        m_StartButton.gameObject.SetActive(m_InOptions);
        m_SfxBar.gameObject.SetActive(!m_InOptions);
        m_MusicBar.gameObject.SetActive(!m_InOptions);
        m_MusicText.gameObject.SetActive(!m_InOptions);
        m_SfxText.gameObject.SetActive(!m_InOptions);
        m_InOptions = !m_InOptions;
    }

    public void ConsumeCharge(int aCharge)
    {
        if(aCharge >= 0)
        {
            m_Charges1[aCharge].gameObject.SetActive(false);
        }

    }

    public void ToggleGaugePrompt(bool aBool)
    {
        m_GaugePrompt1.gameObject.SetActive(aBool);
    }

}
