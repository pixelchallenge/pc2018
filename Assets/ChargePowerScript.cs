﻿using UnityEngine;
using UnityEngine.UI;

public class ChargePowerScript : MonoBehaviour {

    public LeafBlower Blower;
    public FeedbackPlayer GarbageDay;
    public FeedbackPlayer GroceryDelivery;
    public FeedbackPlayer GoingPostal;
    public FeedbackPlayer PaperOverRock;
    public FeedbackPlayer JohnCena;

    public Slider m_PowerUpSlider;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            //Blower.UltimateAmount += 0.07f;

            //if(Blower.UltimateAmount >= 1)
            //{
            //    Blower.UltimateAmount = 1;
            //}

            //m_PowerUpSlider.value = Blower.UltimateAmount;


            if (other.gameObject.name.Contains("Garbage"))
            {
                GarbageDay.Show();
            }
            if (other.gameObject.name.Contains("Grocery"))
            {
                GroceryDelivery.Show();
            }
            if (other.gameObject.name.Contains("Letter"))
            {
                GoingPostal.Show();
            }
            if (other.gameObject.name.Contains("Paper"))
            {
                PaperOverRock.Show();
            }
            if (other.gameObject.name.Contains("Civilian"))
            {
                PaperOverRock.Show();
            }
        }
    }
}
