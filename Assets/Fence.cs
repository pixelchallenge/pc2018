﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fence : MonoBehaviour {

    public Rigidbody Rigidbody;

    public void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 9 && other.gameObject.layer != 8 || other.gameObject.GetComponentInParent<Rigidbody>().velocity.magnitude < 8f)
            return;

        Rigidbody.isKinematic = false;

    }
}
