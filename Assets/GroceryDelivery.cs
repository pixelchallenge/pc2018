﻿using TMPro;
using UnityEngine;

public class GroceryDelivery : MonoBehaviour {

    public Vector3 StartingPosition;
    public RectTransform RectTransform;
    public TextMeshPro textMesh;
    public bool IsShowing = false;
    public float ShowSpeed = 10f;
    public float MoveSpeed = 10f;

	public void Start () {
        IsShowing = false;
        StartingPosition = RectTransform.localPosition;
	}
	
	public void Update () {
        if (!IsShowing)
            return;

        if (textMesh.color.a < 1)
            textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, textMesh.color.a + (ShowSpeed * Time.deltaTime));
        else
        {
            if (RectTransform.localPosition.z < 20)
                RectTransform.localPosition = new Vector3(RectTransform.localPosition.x, RectTransform.localPosition.y, RectTransform.localPosition.z + MoveSpeed * Time.deltaTime) ;
            else
            {
                textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, 0);
                RectTransform.localPosition = StartingPosition;
                IsShowing = false;
            }
        }
    }

    public void Show()
    {
        IsShowing = true;
    }
}
