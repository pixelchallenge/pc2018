﻿using TMPro;
using UnityEngine;

public class FeedbackPlayer : MonoBehaviour {

    public Vector3 StartingPosition;
    public RectTransform RectTransform;
    public TextMeshPro textMesh;
    public bool IsShowing = false;
    public float ShowSpeed = 10f;
    public float MoveSpeed = 10f;
    public float Acceleration = 0.5f;

    public void Start()
    {
        IsShowing = false;
        
        RectTransform = GetComponent<RectTransform>();
        textMesh = GetComponent<TextMeshPro>();
        StartingPosition = RectTransform.localPosition;
    }

    public void Update()
    {
        if (!IsShowing)
            return;

        if (textMesh.color.a < 0.5f)
        {
            textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, textMesh.color.a + (ShowSpeed * Time.deltaTime));
        }
        else
        {
            if (textMesh.color.a < 1f)
                textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, textMesh.color.a + (ShowSpeed * Time.deltaTime));

            if (RectTransform.localPosition.z < 20)
            {
                RectTransform.localPosition = new Vector3(RectTransform.localPosition.x, RectTransform.localPosition.y, RectTransform.localPosition.z + (MoveSpeed + Acceleration) * Time.deltaTime);
                Acceleration += 0.5f;
            }
            else
            {
                textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, 0);
                RectTransform.localPosition = StartingPosition;
                RectTransform.localScale = new Vector3(1, 1, 1);
                Acceleration = 0.5f;
                IsShowing = false;
            }
        }
    }

    public void Show()
    {
        IsShowing = true;
    }
}
