/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BLOWER1CHARTRIGGERPUSHED = 2860647992U;
        static const AkUniqueID BLOWER1ENGINESTARTS = 13425416U;
        static const AkUniqueID BLOWER1ENGINESTOPS = 103209562U;
        static const AkUniqueID BLOWER2CHARTRIGGERPUSHED = 3083457141U;
        static const AkUniqueID BLOWER2ENGINESTARTS = 1804561359U;
        static const AkUniqueID BLOWER2ENGINESTOPS = 3641228715U;
        static const AkUniqueID DOORBREAK = 1282816654U;
        static const AkUniqueID WINDOWBREAK = 2927830534U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace WALKSURFACETYPE
        {
            static const AkUniqueID GROUP = 3891673931U;

            namespace SWITCH
            {
                static const AkUniqueID CONCRETE = 841620460U;
                static const AkUniqueID DIRT = 2195636714U;
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace WALKSURFACETYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID TRIGGERPLAYER1 = 1776285501U;
        static const AkUniqueID TRIGGERPLAYER2 = 1776285502U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID PIXELSOUNDBANK = 2893370910U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID EXTERIORREVERB = 3720409019U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID COMMUNICATION = 530303819U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
