﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Truck : MonoBehaviour {

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Player") || collision.gameObject.name.Contains("Civil"))
            AkSoundEngine.PostEvent("Cartoony_BodyHit", gameObject);
    }
}
