﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject UIInGame;

    public bool m_RoundStarted = false;
    public int PlayerOneRoundVictories = 0;
    public int PlayerTwoRoundVictories = 0;

    public List<GameObject> TrashsPlayerOne = new List<GameObject>();
    public List<GameObject> TrashsPlayerTwo = new List<GameObject>();

    public UIAnimationScript Countdown;

    public GameObject PlayerOneWinner;
    public GameObject PlayerOneLoser;
    public GameObject PlayerTwoWinner;
    public GameObject PlayerTwoLoser;

    public TextMeshProUGUI CountDowntext;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);

        Time.timeScale = 0;
    }

    public void StartGame()
    {
        Time.timeScale = 1;
        UIInGame.SetActive(true);
    }

    public IEnumerator CountDownBeforeGame(string musicEvent)
    {
        CountDowntext.text = "3";
        CountDowntext.gameObject.SetActive(true);

        float totalTime = 3;
        AkSoundEngine.PostEvent("CountdownStartRound_123", gameObject);
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSecondsRealtime(1);
            totalTime -= 1;
            var integer = (int)totalTime;
            CountDowntext.text = integer.ToString();
            AkSoundEngine.PostEvent("CountdownStartRound_123", gameObject);
        }
        
        GameManager.Instance.StartGame();
        CountDowntext.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
        AkSoundEngine.PostEvent("CountdownStartRound_GO", gameObject);
        AkSoundEngine.PostEvent(musicEvent, gameObject);
        yield return null;
    }

    public void VictoryConditions()
    {
        if(TrashsPlayerOne.Count == TrashsPlayerTwo.Count)
        {
            // Draw
        }
        else if(TrashsPlayerOne.Count > TrashsPlayerTwo.Count)
        {
            PlayerTwoRoundVictories++;
        }
        else
        {
            PlayerOneRoundVictories++;
        }

        if (PlayerOneRoundVictories == 2)
        {
            //Show victory

            //Show Restart
        }

        if (PlayerTwoRoundVictories == 2)
        {
            //Show victory
            //Show restart
        }

        Countdown.Show();
        Invoke("NewRound", 5f);
    }

    public void NewRound()
    {
        TrashsPlayerOne.Clear();
        TrashsPlayerTwo.Clear();
        RestartManager.Instance.RestartLevel();
    }
}
