﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashDetector : MonoBehaviour
{
    public BoxCollider Collider;

    public void Start()
    {
        Collider = GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Trash" && gameObject.name == "Player1 Detector")
        {
            
            GameManager.Instance.TrashsPlayerOne.Add(other.gameObject);
            
        }
        else if (other.tag == "Trash" && gameObject.name == "Player2 Detector")
        {
            GameManager.Instance.TrashsPlayerTwo.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Trash" && gameObject.name == "Player1 Detector")
        {
            GameManager.Instance.TrashsPlayerOne.Remove(other.gameObject);
        }
        else if (other.tag == "Trash" && gameObject.name == "Player2 Detector")
        {
            GameManager.Instance.TrashsPlayerTwo.Remove(other.gameObject);
        }
    }
}
