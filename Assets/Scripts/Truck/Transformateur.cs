﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Transformateur : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_CountDownText;
    [SerializeField]
    private CameraShake m_CameraShake;
    private Canvas m_Canvas;
    [SerializeField]
    private List<GameObject> m_TransformateurMeshes;
    public List<Rigidbody> ObjectExplosion = new List<Rigidbody>();
    public GameObject m_ThunderVFX;
    public GameObject Thunder;
    public GameObject Spark;
    public GameObject Smoke;

    void Start ()
    {
        m_CameraShake = Camera.main.gameObject.GetComponent<CameraShake>();
        StartCoroutine(CountDown());
	}
	
	void Update ()
    {
		
	}

    private IEnumerator CountDown()
    {
        float duration = 0f;
        float totalTime = 6;
        while (totalTime >= duration)
        {
            totalTime -= Time.deltaTime;
            var integer = (int)totalTime;
            m_CountDownText.text = integer.ToString();
            yield return null;
        }
        m_CameraShake.ShakeThecam();

        m_ThunderVFX.SetActive(true);
        Thunder.SetActive(true);
        Spark.SetActive(true);
        Smoke.SetActive(true);


        //Verify which player score it will affet

        foreach(var obj in ObjectExplosion)
        {
            obj.AddExplosionForce(1000f, transform.position, 1000f, 4);
        }
        AkSoundEngine.PostEvent("ThunderStrike", gameObject);


        foreach (GameObject obj in m_TransformateurMeshes)
        {
            obj.SetActive(false);
        }

        Invoke("DestroyGameObject", 5f);
    }

    public void OnTriggerEnter(Collider other)
    {
        var rb = other.gameObject.GetComponentInParent<Rigidbody>();
        if (rb != null && !ObjectExplosion.Contains(rb))
            ObjectExplosion.Add(rb);
    }

    public void OnTriggerExit(Collider other)
    {
        var rb = other.gameObject.GetComponentInParent<Rigidbody>();
        if (rb != null && !ObjectExplosion.Exists(x => rb == x))
            ObjectExplosion.Remove(rb);
    }

    public void DestroyGameObject()
    {
        Destroy(gameObject);
    }
}
