﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootTrashes : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_TrashToShoot;

    [SerializeField]
    private List<GameObject> m_ListofTrashInGame;

    [SerializeField]
    private GameObject m_Transformateur;

    private int m_NumberOfElements;

    public bool m_IsGoingToDropTransformateur = false;

    public bool m_IsEmitterBack = false;
    public bool m_IsEmitterRight = false;
    public bool m_IsEmitterLeft = false;

    void Start ()
    {
        m_NumberOfElements = m_TrashToShoot.Capacity;
	}

    public void DeverseTrash()
    {
        StartCoroutine(NumberOfTrashPerTime());
        StartCoroutine(TryToDropTransformateur());
    }

    private IEnumerator TryToDropTransformateur()
    {
        yield return new WaitForSeconds(1f);

        if(m_IsGoingToDropTransformateur)
        {
            Instantiate(m_Transformateur, transform.position, Quaternion.identity);
        }

        m_IsGoingToDropTransformateur = false;
    }

    private IEnumerator NumberOfTrashPerTime()
    {
        yield return new WaitForSeconds(0.8f);

        if(m_IsEmitterBack)
        {
            for (int i = 0; i < 2; i++)
            {
                m_ListofTrashInGame.Add(Instantiate(m_TrashToShoot[Random.Range(0, m_NumberOfElements)], transform.position + new Vector3(Random.Range(-10, 10f), Random.Range(0, 5f), 0f), Quaternion.identity));
                m_ListofTrashInGame.Add(Instantiate(m_TrashToShoot[Random.Range(0, m_NumberOfElements)], transform.position + new Vector3(Random.Range(-10, 10f), Random.Range(0, 5f), 0f), Quaternion.identity));
                yield return new WaitForSeconds(Random.Range(0.0001f, 0.0002f));
            }
        }

        else if (m_IsEmitterLeft)
        {
            for (int i = 0; i < 2; i++)
            {
                m_ListofTrashInGame.Add(Instantiate(m_TrashToShoot[Random.Range(0, m_NumberOfElements)], transform.position + new Vector3(0, Random.Range(0, 5f), Random.Range(-5f, -20f)), Quaternion.identity));
                m_ListofTrashInGame.Add(Instantiate(m_TrashToShoot[Random.Range(0, m_NumberOfElements)], transform.position + new Vector3(0, Random.Range(0, 5f), Random.Range(-5f, -20f)), Quaternion.identity));
                yield return new WaitForSeconds(Random.Range(0.0001f, 0.0002f));
            }
        }

        else if (m_IsEmitterRight)
        {
            for (int i = 0; i < 2; i++)
            {
                m_ListofTrashInGame.Add(Instantiate(m_TrashToShoot[Random.Range(0, m_NumberOfElements)], transform.position + new Vector3(Random.Range(-10, 10f), Random.Range(0, 5f), Random.Range(5f, 20f)), Quaternion.identity));
                m_ListofTrashInGame.Add(Instantiate(m_TrashToShoot[Random.Range(0, m_NumberOfElements)], transform.position + new Vector3(Random.Range(-10, 10f), Random.Range(0, 5f), Random.Range(5f, 20f)), Quaternion.identity));
                yield return new WaitForSeconds(Random.Range(0.0001f, 0.002f));
            }
        }
    }

    public void RemoveAllTrashes()
    {
        foreach(GameObject obj in m_ListofTrashInGame)
        {
            Destroy(obj.gameObject);
        }

        m_ListofTrashInGame.Clear();
    }
}
