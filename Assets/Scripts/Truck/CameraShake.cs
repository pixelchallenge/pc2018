﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Vector3 m_InitialPosition;

    void Start()
    {
        m_InitialPosition = transform.position;

    }

    public void ShakeThecam()
    {
        StartCoroutine(Shake());
    }

    private IEnumerator Shake()
    {
        for(int i=0; i<20; i++)
        {
            transform.position = new Vector3(transform.position.x + Random.Range(-0.2f, 0.2f), transform.position.y + Random.Range(-0.2f, 0.2f), transform.position.z);
            yield return new WaitForSeconds(0.03f);
            transform.position = m_InitialPosition;
        }
    }
}
