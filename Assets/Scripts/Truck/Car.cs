﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    [SerializeField]
    private ShootTrashes m_ShootTrash;
    [SerializeField]
    private ShootTrashes m_ShootTrash2;
    [SerializeField]
    private ShootTrashes m_ShootTrash3;
    [SerializeField]
    private Timer m_CurrenTime;

    [SerializeField]
    float m_Speed = 10f;


    private Vector3 m_InitialPosition;
    private Rigidbody m_RB;

    private int m_TransformateurCurrentDropChance = 33;

    private float m_FirstPassageMaxTime = 54f;
    private float m_FirstPassageMinimumTime = 46f;

    private float m_SecondPassageMaxTime = 34f;
    private float m_SecondPassageMinimumTime = 20f;

    private float m_ThirdPassageMaxTime = 14f;
    private float m_ThirdPassageMinimumTime = 6f;

    private float m_FirstRandomPassage = 0f;
    private float m_SecondRandomPassage = 0f;
    private float m_ThirdRandomPassage = 0f;

    private bool m_CarIsDriving = false;

    private bool m_FirstPassageDone = false;
    private bool m_SecondPassageDone = false;
    private bool m_ThirdPassageDone = false;

    private bool m_TransformateurHasDropped = false;

    private bool m_HasTriedToDropTransformateur = false;

    void Start()
    {
        m_InitialPosition = transform.position;
        m_RB = GetComponent<Rigidbody>();

        Init();
    }

    public void Init()
    {
        CarRandomTime();
        m_FirstPassageDone = false;
        m_SecondPassageDone = false;
        m_ThirdPassageDone = false;
    }

    void FixedUpdate()
    {
        if (m_CarIsDriving)
            Move();
    }

    private void Move()
    {
        m_RB.velocity = Vector3.right * Time.fixedDeltaTime * m_Speed;
    }

    private void CarRandomTime()
    {
        m_FirstRandomPassage = Random.Range(m_FirstPassageMinimumTime, m_FirstPassageMaxTime);
        m_SecondRandomPassage = Random.Range(m_SecondPassageMinimumTime, m_SecondPassageMaxTime);
        m_ThirdRandomPassage = Random.Range(m_ThirdPassageMinimumTime, m_ThirdPassageMaxTime);
    }

    private void Update()
    {
        if ((UIManager.Instance.TimeLeft <= m_FirstRandomPassage) && m_FirstPassageDone == false && !m_HasTriedToDropTransformateur)
        {
            m_FirstPassageDone = true;

            if (!m_TransformateurHasDropped)
            {
                m_ShootTrash.DeverseTrash();
                m_ShootTrash2.DeverseTrash();
                m_ShootTrash3.DeverseTrash();
                ChanceToDropTransformator();
            }

            m_CarIsDriving = true;
            StartCoroutine(CarReset());
        }
        else if (UIManager.Instance.TimeLeft <= m_SecondRandomPassage && m_SecondPassageDone == false)
        {
            m_SecondPassageDone = true;

            m_HasTriedToDropTransformateur = true;
            if (!m_TransformateurHasDropped)
                ChanceToDropTransformator();

            m_ShootTrash.DeverseTrash();
            m_CarIsDriving = !m_CarIsDriving;
            StartCoroutine(CarReset());
        }
        else if (UIManager.Instance.TimeLeft <= m_ThirdRandomPassage && m_ThirdPassageDone == false)
        {
            m_ThirdPassageDone = true;

            if (!m_TransformateurHasDropped)
                ChanceToDropTransformator();
            m_ShootTrash.DeverseTrash();
            m_CarIsDriving = !m_CarIsDriving;
            StartCoroutine(CarReset());
        }
    }

    private IEnumerator CarReset()
    {
        yield return new WaitForSeconds(6f);
        transform.position = m_InitialPosition;
        m_CarIsDriving = false;
        m_RB.velocity = Vector3.zero;
        m_HasTriedToDropTransformateur = false;

    }

    private void ChanceToDropTransformator()
    {
        int ChanceToDrop = Random.Range(0, 100);



        if (ChanceToDrop <= m_TransformateurCurrentDropChance && !m_TransformateurHasDropped)
        {
            m_TransformateurHasDropped = true;
            m_ShootTrash.m_IsGoingToDropTransformateur = true;
        }
        else
        {
            m_TransformateurCurrentDropChance += 34;
        }
    }
}
