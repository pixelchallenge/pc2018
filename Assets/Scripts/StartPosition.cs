﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPosition : MonoBehaviour
{
    public Vector3 InitialPosition = Vector3.zero;
    public Quaternion InitialRotation = Quaternion.identity;
    public Rigidbody Rigidbody;

	private void Start ()
    {
        InitialPosition = transform.localPosition;
        InitialRotation = transform.localRotation;
        Rigidbody = GetComponentInParent<Rigidbody>();
	}

    public void SetInitialPosition()
    {
        if (Rigidbody != null)
        {
            Rigidbody.isKinematic = true;
            Rigidbody.velocity = Vector3.zero;
        }
            
        transform.localRotation = InitialRotation;
        transform.localPosition = InitialPosition;

        if (Rigidbody != null && !gameObject.name.Contains("Door") && !gameObject.name.Contains("Fence"))
        {
            Rigidbody.isKinematic = false;
            Rigidbody.velocity = Vector3.zero;

        }
    }
}
