﻿using Assets._Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestartManager : MonoBehaviour
{
    [SerializeField]
    List<GameObject> m_ObjectsToReset = new List<GameObject>();
    public List<Window> Windows = new List<Window>();
    public List<Door> Doors = new List<Door>();
    public RagdollManager[] Civilians = new RagdollManager[5];
    public TrashDetector[] Detectors = new TrashDetector[2];
    public Timer Timer;
    public Car DumpTruck;

    private static RestartManager _instance;

    public static RestartManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<RestartManager>();
            }
            return _instance;
        }
    }

    public void Start()
    {
        m_ObjectsToReset = GameObject.FindGameObjectsWithTag("Trash").ToList();
        m_ObjectsToReset.AddRange(GameObject.FindGameObjectsWithTag("Player").ToList());
        Windows = GameObject.FindGameObjectsWithTag("Window").Select(go => go.GetComponent<Window>()).ToList();
        Doors = GameObject.FindGameObjectsWithTag("Door").Select(go => go.GetComponent<Door>()).ToList();
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);

    }

    public void RestartLevel()
    {
        GameManager.Instance.UIInGame.SetActive(false);
        Time.timeScale = 0;

        ResetObjects();
        RestartTimer();
        UIManager.Instance.RestartUI();

        foreach (var detector in Detectors)
        {
            detector.Collider.enabled = true;
        }

        var winCount = GameManager.Instance.PlayerOneRoundVictories + GameManager.Instance.PlayerTwoRoundVictories;
        if (GameManager.Instance.PlayerOneRoundVictories == 2 || GameManager.Instance.PlayerTwoRoundVictories == 2)
            SceneManager.LoadScene(0);

        if (winCount == 1)
            StartCoroutine(GameManager.Instance.CountDownBeforeGame("StartMusic2ndRound"));
        if (winCount == 2)
            StartCoroutine(GameManager.Instance.CountDownBeforeGame("StartMusic3rdRound"));
        
        
        foreach(var st in DumpTruck.gameObject.GetComponentsInChildren<ShootTrashes>())
        {
            st.RemoveAllTrashes();
        }

        DumpTruck.Init();
    }

    private void ResetObjects()
    {
        foreach(var civilian in Civilians)
        {
            civilian.Desactivate();
        }

        foreach(var detector in Detectors)
        {
            detector.Collider.enabled = false;
        }

        foreach (GameObject obj in m_ObjectsToReset)
        {
            var objStartPosition = obj.GetComponentInParent<StartPosition>();
            if (objStartPosition != null)
                objStartPosition.SetInitialPosition();
        }

        foreach(var window in Windows)
        {
            window.Repair();
        }

        foreach(var door in Doors)
        {
            door.Repair();
        }
    }

    private void RestartTimer()
    {
        Timer.StartTimer();

    }
}
