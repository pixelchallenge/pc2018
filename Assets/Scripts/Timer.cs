﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_meText;
    public bool IsTicking;

    public void Start()
    {
        StartTimer();
    }

    public void StartTimer()
    {
        UIManager.Instance.TimeLeft = 60f;
        IsTicking = true;
    }

    private void Update()
    {
        if (!IsTicking)
            return;

        m_meText.text = "00:" + ((int)UIManager.Instance.TimeLeft).ToString("D2");

        UIManager.Instance.TimeLeft -= Time.deltaTime;
        if (UIManager.Instance.TimeLeft < 0)
        {
            UIManager.Instance.TimeLeft = 0;

            GameManager.Instance.VictoryConditions();
            IsTicking = false;
        }
    }
}
