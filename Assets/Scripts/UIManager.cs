﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public  TextMeshProUGUI PlayerOneTrashUI;
    public  TextMeshProUGUI PlayerTwoTrashUI;
    public UIAnimationScript Countdown;
    public float TimeLeft = 5f;

    private const float m_RoundTime = 59f;

    private static UIManager _instance;

    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIManager>();
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);

    }

    private void Start()
    {
        TimeLeft = m_RoundTime;
    }

    public void FixedUpdate()
    {
        UIManager.Instance.PlayerOneTrashUI.text = GameManager.Instance.TrashsPlayerOne.Count.ToString();
        UIManager.Instance.PlayerTwoTrashUI.text = GameManager.Instance.TrashsPlayerTwo.Count.ToString();
    }

    public void RestartUI()
    {
        TimeLeft = m_RoundTime;
        Countdown.Hide();
    }

    public float GetRoundTIme()
    {
        return m_RoundTime;
    }
}
