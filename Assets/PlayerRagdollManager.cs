﻿using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerRagdollManager : RagdollManager {

    public GameObject[] Ragdolls;
    public GameObject[] Anims;

    public override void Activate()
    {
        foreach(var ragdoll in Ragdolls)
        {
            ragdoll.SetActive(true);
        }

        foreach(var anim in Anims)
        {
            anim.SetActive(false);
        }

        foreach (var c in RagdollColliders)
        {
            c.enabled = true;
        }

        foreach (var rb in RagdollRigidbodies)
        {
            rb.isKinematic = false;
            rb.useGravity = true;
        }

        foreach (var up in UpdatePosition)
        {
            up.enabled = true;
        }


        var animator = MainEntity.GetComponent<Animator>();
        var collider = MainEntity.GetComponent<Collider>();
        var rigidbody = MainEntity.GetComponent<Rigidbody>();
        var character = MainEntity.GetComponent<PlayerCharacter>();
        var controller = MainEntity.GetComponent<ThirdPersonUserControl>();
        if (animator != null)
            animator.enabled = false;
        if (collider != null)
            collider.enabled = false;
        if (rigidbody != null)
        {
            rigidbody.isKinematic = true;
            rigidbody.useGravity = false;
        }
        if (character != null)
            character.enabled = false;
        if (controller != null)
            controller.enabled = false;
    }

    public override void Desactivate()
    {
        foreach (var ragdoll in Ragdolls)
        {
            ragdoll.SetActive(false);
        }

        foreach (var anim in Anims)
        {
            anim.SetActive(true);
        }

        foreach (var c in RagdollColliders)
        {
            c.enabled = false;
        }

        foreach (var rb in RagdollRigidbodies)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }
        
        foreach(var trans in RagdollTransforms)
        {
            trans.localPosition = Positions[trans.gameObject];
            trans.localRotation = Rotations[trans.gameObject];
        }

        var animator = MainEntity.GetComponent<Animator>();
        var collider = MainEntity.GetComponent<Collider>();
        var rigidbody = MainEntity.GetComponent<Rigidbody>();
        var character = MainEntity.GetComponent<PlayerCharacter>();
        var controller = MainEntity.GetComponent<ThirdPersonUserControl>();
        if (animator != null)
            animator.enabled = true;
        if (collider != null)
            collider.enabled = true;
        if (rigidbody != null)
        {
            rigidbody.isKinematic = false;
            rigidbody.useGravity = true;
        }
        if (character != null)
            character.enabled = true;
        if (controller != null)
            controller.enabled = true;
    }
}
